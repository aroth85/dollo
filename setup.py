import numpy
import versioneer

from Cython.Build import cythonize
from setuptools import setup, find_packages, Extension


extensions = [
    Extension(
        name='dollo.recursion',
        sources=['dollo/recursion.pyx'],
        include_dirs=[numpy.get_include()],
    ),
]

setup(
    author='Andrew McPhershon, Andrew Roth',
    author_email='andrew.mcpherson@gmail.com, andrewjlroth@gmail.com',
    name='PyDollo',
    description='Simple stochastic Dollo phylogenetic model for analysing cancer genomics data.',
    url='http://compbio.bccrc.ca',
    version=versioneer.get_version(),
    cmdclass=versioneer.get_cmdclass(),

    entry_points={'console_scripts': ['PyDollo = dollo.cli:main']},
    ext_modules=cythonize(extensions),
    packages=find_packages(),
)
